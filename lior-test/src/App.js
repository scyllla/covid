import React, { Component } from 'react'
import Clients from './component/Clients'
import ClientForm from './component/ClientForm'

class App extends Component {

  state = {
    clients: [
      { id: 1, nom: "scylla diakite" },
      { id: 2, nom: "kaly diakite" },
      { id: 3, nom: "kande sidibe" }
    ]
  }
  handleAdd = (client) => {
    const clients = this.state.clients.slice()
    clients.push(client)
    this.setState({
      clients
    })
  }

  handleDelete = (id) => {
    const clients = [...this.state.clients]
    const index = clients.findIndex((client) => client.id === id)
    clients.splice(index, 1)
    this.setState({
      clients,

    })
  }
  render() {

    return (

      <div>
        <h1>Liste des clients</h1>
        <Clients onDelete={this.handleDelete} clients={this.state.clients} />
        <ClientForm onClientAdd={this.handleAdd} />

      </div>
    )
  }
}
export default App
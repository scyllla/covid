import React, { Component } from 'react'

class ClientForm extends Component {
    state = {
        nouveauClient: ''
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const id = new Date().getTime()
        const nom = this.state.nouveauClient

        this.props.onClientAdd({ id, nom })
        this.setState({
            nouveauClient: ''
        })
    }

    handleChange = (event) => {
        this.setState({
            nouveauClient: event.target.value
        })
    }

    render() {

        return (
            <form onSubmit={this.handleSubmit}>
                <input onChange={this.handleChange} value={this.state.nouveauClient} type="text" placeholder="Ajouter un client" />
                <button type="submit">Confirmer</button>
            </form>
        )
    }
}
export default ClientForm

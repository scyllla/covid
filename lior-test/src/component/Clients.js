import React from 'react'

const Clients = ({ clients, onDelete }) => {

    return (
        <ul>
            {clients.map((client) => {
                return <li>{client.nom} <button onClick={() => onDelete(client.id)} >X</button> </li>
            })}
        </ul>
    )
}
export default Clients

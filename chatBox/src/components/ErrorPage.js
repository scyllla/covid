import React from 'react'

function ErrorPage() {
    return (
        <h1 className="notFound">
            404 page not found
        </h1>
    )
}

export default ErrorPage

import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyBL6cnzP20qSZ_ThHQSTSABzvTcaEDFnog",
    authDomain: "chatbox-5d89c.firebaseapp.com",
    databaseURL: "https://chatbox-5d89c.firebaseio.com",
})

const base = Rebase.createClass(firebase.database())

export {
    firebaseApp
}

export default base

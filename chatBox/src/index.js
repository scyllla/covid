import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import ErrorPage from './components/ErrorPage'
import Connexion from './components/Connexion'
import App from './App'

import { BrowserRouter, Route, Switch } from 'react-router-dom'

const Root = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Connexion}></Route>
            <Route path="/pseudo/:pseudo" component={App}> </Route>
            <Route component={ErrorPage}></Route>
        </Switch>
    </BrowserRouter>
)







ReactDOM.render(<Root />, document.getElementById('root'))
